﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace StockAnalyze
{
    public partial class Form1 : Form
    {
        #region Series
        Series S_Stk = new Series();
        Series S_Vol = new Series();
        Series S_StkMA60 = new Series();
        Series S_StkMA = new Series();
        Series S_VolMA20 = new Series();

        Series S_RangeTop = new Series();
        Series S_RangeBot = new Series();

        Series S_MACD = new Series();
        Series S_CCI = new Series();
        Series S_RSI = new Series();
        #endregion

        public Form1()
        {
            InitializeComponent();

            #region basic setting for chart
            //MainChart.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            //MainChart.ChartAreas[0].AxisY.MajorTickMark.Enabled = false;
            //MainChart.ChartAreas[0].AxisY.Minimum = 4000;

            S_Stk.ChartType = SeriesChartType.Candlestick;
            S_Vol.ChartType = SeriesChartType.Column;
            S_StkMA60.ChartType = SeriesChartType.Line;
            S_StkMA.ChartType = SeriesChartType.Line;
            S_VolMA20.ChartType = SeriesChartType.Line;
            S_MACD.ChartType = SeriesChartType.Column;
            S_CCI.ChartType = SeriesChartType.Column;
            S_RSI.ChartType = SeriesChartType.Column;

            S_RangeTop.ChartType = SeriesChartType.StepLine;
            S_RangeBot.ChartType = SeriesChartType.StepLine;

            MainChart.Series.Add(S_Stk);
            MainChart.Series.Add(S_Vol);
            MainChart.Series.Add(S_StkMA60);
            MainChart.Series.Add(S_StkMA);
            MainChart.Series.Add(S_VolMA20);
            MainChart.Series.Add(S_MACD);
            MainChart.Series.Add(S_CCI);
            MainChart.Series.Add(S_RSI);

            MainChart.Series.Add(S_RangeTop);
            MainChart.Series.Add(S_RangeBot);

            S_Stk.ChartArea = "StockArea";
            S_Vol.ChartArea = "VolumeArea";
            S_StkMA60.ChartArea = "StockArea";
            S_StkMA.ChartArea = "StockArea";
            S_VolMA20.ChartArea = "VolumeArea";
            S_MACD.ChartArea = "MACDArea";
            S_CCI.ChartArea = "CCIArea";
            S_RSI.ChartArea = "RSIArea";

            S_RangeTop.ChartArea = "StockArea";
            S_RangeBot.ChartArea = "StockArea";

            //跳過空白點
            S_Stk.IsXValueIndexed = true;
            S_Vol.IsXValueIndexed = true;
            S_StkMA60.IsXValueIndexed = true;
            S_StkMA.IsXValueIndexed = true;
            S_VolMA20.IsXValueIndexed = true;
            S_MACD.IsXValueIndexed = true;
            S_CCI.IsXValueIndexed = true;
            S_RSI.IsXValueIndexed = true;

            S_RangeTop.IsXValueIndexed = true;
            S_RangeBot.IsXValueIndexed = true;

            //圖表特殊設定
            S_Stk.CustomProperties = "PointWidth=0.5";

            //圖表顏色
            S_Stk.BackSecondaryColor = Color.Black;
            S_Stk.Color = Color.Aqua;
            S_Stk.BorderColor = Color.Aqua;
            
            S_Vol.Color = Color.Yellow;
            S_StkMA60.Color = Color.Orange;
            S_StkMA.Color = Color.LawnGreen;
            S_VolMA20.Color = Color.Orange;
            S_MACD.Color = Color.Aqua;
            S_CCI.Color = Color.Aqua;
            S_RSI.Color = Color.Aqua;

            S_RangeTop.Color = Color.Transparent;
            S_RangeBot.Color = Color.Transparent;

            //格線樣式
            MainChart.ChartAreas["StockArea"].AxisY.MajorGrid.LineColor = Color.DimGray;
            MainChart.ChartAreas["StockArea"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            MainChart.ChartAreas["StockArea"].AxisX.MajorGrid.LineColor = Color.Transparent;
            MainChart.ChartAreas["VolumeArea"].AxisY.MajorGrid.LineColor = Color.DimGray;
            MainChart.ChartAreas["VolumeArea"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            MainChart.ChartAreas["VolumeArea"].AxisX.MajorGrid.LineColor = Color.Transparent;
            MainChart.ChartAreas["MACDArea"].AxisY.MajorGrid.LineColor = Color.DimGray;
            MainChart.ChartAreas["MACDArea"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            MainChart.ChartAreas["MACDArea"].AxisX.MajorGrid.LineColor = Color.Transparent;
            MainChart.ChartAreas["CCIArea"].AxisY.MajorGrid.LineColor = Color.DimGray;
            MainChart.ChartAreas["CCIArea"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            MainChart.ChartAreas["CCIArea"].AxisX.MajorGrid.LineColor = Color.Transparent;
            MainChart.ChartAreas["RSIArea"].AxisY.MajorGrid.LineColor = Color.DimGray;
            MainChart.ChartAreas["RSIArea"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            MainChart.ChartAreas["RSIArea"].AxisX.MajorGrid.LineColor = Color.Transparent;

            MainChart.ChartAreas["StockArea"].AxisY.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["StockArea"].AxisX.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["VolumeArea"].AxisY.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["VolumeArea"].AxisX.LabelStyle.ForeColor = Color.Transparent;
            MainChart.ChartAreas["MACDArea"].AxisY.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["MACDArea"].AxisX.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["CCIArea"].AxisY.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["CCIArea"].AxisX.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["RSIArea"].AxisY.LabelStyle.ForeColor = Color.White;
            MainChart.ChartAreas["RSIArea"].AxisX.LabelStyle.ForeColor = Color.White;

            //X軸設定
            //S_Stk.XValueType = ChartValueType.DateTime;
            //S_Vol.XValueType = ChartValueType.DateTime;
            //S_StkMA60.XValueType = ChartValueType.DateTime;
            //S_VolMA20.XValueType = ChartValueType.DateTime;
            //MainChart.ChartAreas["StockArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd\nHH:mm";
            //MainChart.ChartAreas["VolumeArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd\nHH:mm";

            MainChart.ChartAreas["VolumeArea"].AlignWithChartArea = "StockArea";
            MainChart.ChartAreas["MACDArea"].AlignWithChartArea = "StockArea";
            MainChart.ChartAreas["CCIArea"].AlignWithChartArea = "StockArea";
            MainChart.ChartAreas["RSIArea"].AlignWithChartArea = "StockArea";

            MainChart.ChartAreas["CCIArea"].AxisY.Minimum = -300;
            MainChart.ChartAreas["CCIArea"].AxisY.Maximum = 300;
            MainChart.ChartAreas["CCIArea"].AxisY.Interval = 100;

            //按鈕與圈選功能
            MainChart.ChartAreas["StockArea"].CursorX.IsUserSelectionEnabled = true;
            MainChart.ChartAreas["VolumeArea"].CursorX.IsUserSelectionEnabled = true;
            MainChart.ChartAreas["MACDArea"].CursorX.IsUserSelectionEnabled = true;
            MainChart.ChartAreas["CCIArea"].CursorX.IsUserSelectionEnabled = true;
            MainChart.ChartAreas["RSIArea"].CursorX.IsUserSelectionEnabled = true;

            //框線間隔
            //int AxisXInterval = 10;
            //MainChart.ChartAreas["StockArea"].AxisX.Interval = AxisXInterval;
            //MainChart.ChartAreas["VolumeArea"].AxisX.Interval = AxisXInterval;
            //MainChart.ChartAreas["StockArea"].AxisX.Minimum = 1;
            //MainChart.ChartAreas["StockArea"].AxisX.Maximum = 50;

            //scroll bar與 最小值
            MainChart.ChartAreas["StockArea"].AxisX.ScrollBar.Axis.Minimum = 0;
            MainChart.ChartAreas["StockArea"].AxisY.ScrollBar.Enabled = false;
            MainChart.ChartAreas["VolumeArea"].AxisX.ScrollBar.Axis.Minimum = 0;
            MainChart.ChartAreas["VolumeArea"].AxisY.ScrollBar.Enabled = false;
            //MainChart.ChartAreas["MACDArea"].AxisX.ScrollBar.Axis.Minimum = 0;
            //MainChart.ChartAreas["MACDArea"].AxisY.ScrollBar.Enabled = false;

            //scroll bar樣式
            MainChart.ChartAreas["StockArea"].AxisX.ScrollBar.Size = 20;
            MainChart.ChartAreas["StockArea"].AxisX.ScrollBar.BackColor = Color.Black;
            MainChart.ChartAreas["StockArea"].AxisX.ScrollBar.ButtonColor = Color.DimGray;
            MainChart.ChartAreas["VolumeArea"].AxisX.ScrollBar.Size = 20;
            MainChart.ChartAreas["VolumeArea"].AxisX.ScrollBar.BackColor = Color.Black;
            MainChart.ChartAreas["VolumeArea"].AxisX.ScrollBar.ButtonColor = Color.DimGray;

            // scale view 顯示範圍
            double scrollSize = 3;
            double viewSize = 100;
            MainChart.ChartAreas["StockArea"].AxisX.ScaleView.SmallScrollSize = scrollSize;
            MainChart.ChartAreas["StockArea"].AxisX.ScaleView.SmallScrollMinSize = scrollSize;
            MainChart.ChartAreas["StockArea"].AxisX.ScaleView.Size = viewSize;
            MainChart.ChartAreas["VolumeArea"].AxisX.ScaleView.SmallScrollSize = scrollSize;
            MainChart.ChartAreas["VolumeArea"].AxisX.ScaleView.SmallScrollMinSize = scrollSize;
            MainChart.ChartAreas["VolumeArea"].AxisX.ScaleView.Size = viewSize;

            //ChartArea設定
            //chart1.ChartAreas[0].Position.Y = 10;
            //chart1.ChartAreas[0].Position.Height = 60;
            //chart1.ChartAreas[1].Position.Y = chart1.ChartAreas[0].Position.Bottom;
            //chart1.ChartAreas[1].Position.Height = 20;
            hide_allIndex();
            MainChart.ChartAreas[combo_index.Text + "Area"].Visible = true;
            #endregion

            textBox_start.Text = startLoadCount.ToString();
            textBox_maxCnt.Text = maxLoadCount.ToString();

            LoadHistoryFile(Scr.MTXday);
        }

        int maxLoadCount = 60000;
        int startLoadCount = 2090;
        //int startLoadCount = 17130; //60min
        #region LoadHistoryFile()
        enum Scr
        {
            TX30min,
            TX1hr,
            TXday,
            //TX1min,
            MTX30min,
            MTX1hr,
            MTXday
        }
        int candle_cnt = 1;
        void LoadHistoryFile(Scr scr)
        {
            string fileName="";
            switch (scr)
            {
                case Scr.MTX30min:
                    fileName = "MTX-30min.txt";
                    break;
                case Scr.MTX1hr:
                    fileName = "MTX-1hr.txt";
                    break;
                case Scr.MTXday:
                    fileName = "MTX-day.txt";
                    break;
                case Scr.TX30min:
                    fileName = "TX-30min.txt";
                    break;
                case Scr.TX1hr:
                    fileName = "TX-1hr.txt";
                    break;
                case Scr.TXday:
                    fileName = "TX-day.txt";
                    break;
                //case Scr.TX1min:
                //    fileName = "WTXF1MA_010102_140919.txt";
                //    break;
            }
            
            string[] lines = System.IO.File.ReadAllLines(fileName);

            candle_cnt = 1;

            AnalysisResultClear();
            ClearChart();
            XAxisSetDateTime((Scr.MTXday == scr) ? true : false);
            _Offset = 150;
            _VolOffset = 0.4;

            DateTime nextPayDay = new DateTime();
            bool isSetNextDay = false;

            for (int i = Int32.Parse(textBox_start.Text); i < lines.Length; i++)
            {
                string[] res = lines[i].Split(',');
                DateTime dtTmp = DateTime.Parse(res[0] +" "+ res[1]);

                AddCandle(candle_cnt, dtTmp, Convert.ToDouble(res[3]), Convert.ToDouble(res[4]),
                    Convert.ToDouble(res[2]), Convert.ToDouble(res[5]));
                AddVolume(candle_cnt, dtTmp, Convert.ToInt32(res[6]));

                //if ((("13:45:00" == res[1]) || (i + 1 != lines.Length && lines[i + 1].Split(',')[0] != res[0]))
                //    && ((int)dtTmp.DayOfWeek == 3 && dtTmp.Day >= 15 && dtTmp.Day <= 21))
                //{
                //    AddStraightLine(candle_cnt, Color.DimGray, 0.02);
                //}
                if (!isSetNextDay)
                {
                    nextPayDay = DaysBeforePayDay(dtTmp);
                    isSetNextDay = true;
                }
                else if ((((Scr.MTXday == scr || Scr.TXday == scr) && "13:45:00" == res[1])
                    || ((Scr.MTX30min == scr || Scr.TX30min == scr) && "13:15:00" == res[1])
                    || ((Scr.MTX1hr == scr || Scr.TX1hr == scr)  && "12:45:00" == res[1]))
                    && nextPayDay <= dtTmp && isSetNextDay)
                {
                    AddStraightLine(candle_cnt, Color.Navy, 0.02, true);
                    isSetNextDay = false;
                }

                candle_cnt++;
                if (S_Stk.Points.Count == Int32.Parse(textBox_maxCnt.Text)) break;
            }
            AddFinanceLine();
            SetAxisViewged();
        }

        /*#region LoadHistoryFileDaily()
        void LoadHistoryFileDaily()
        {
            string[] lines = System.IO.File.ReadAllLines(fileName);

            int candle_cnt = 1;
            double dayHigh = -1, dayLow = 999999, dayOpen=0, dayClose=0;
            int dayVol = 0;

            XAxisSetDate();
            AnalysisResultClear();
            ClearChart();
            _Offset = 50;
            _VolOffset = 0.8;

            //foreach (string line in lines)
            for (int i = startLoadCount; i < lines.Length; i++)
            {
                //string[] res = line.Split(',');
                string[] res = lines[i].Split(',');

                dayVol += Convert.ToInt32(res[6]);
                if (Convert.ToDouble(res[3]) > dayHigh) dayHigh = Convert.ToDouble(res[3]);
                if (Convert.ToDouble(res[4]) < dayLow) dayLow = Convert.ToDouble(res[4]);
                if (dayOpen == 0) dayOpen = Convert.ToDouble(res[2]);
                else if ( ("13:45:00" == res[1])
                    || (i + 1 != lines.Length && lines[i + 1].Split(',')[0] != res[0]))
                {
                    dayClose = Convert.ToDouble(res[5]);

                    DateTime dtTmp = DateTime.Parse(res[0] + " " + res[1]);
                    AddCandle(candle_cnt, dtTmp, dayHigh, dayLow, dayOpen, dayClose);
                    AddVolume(candle_cnt, dtTmp, dayVol);

                    //if ((int)dtTmp.DayOfWeek == 3 && dtTmp.Day >= 15 && dtTmp.Day <= 21)
                    //{
                    //    AddStraightLine(candle_cnt, Color.DimGray, 0.02);
                    //}

                    dayHigh = -1;
                    dayLow = 999999;
                    dayOpen = 0;
                    dayClose = 0;
                    dayVol = 0;

                    candle_cnt++;
                    if (S_Stk.Points.Count == maxLoadCount) break;
                }
            }
            AddFinanceLine();
            SetAxisViewged();
        }
        #endregion
        */
        #endregion

        #region Add Point, Line
        void AddCandle(int no, DateTime DT, double high, double low, double open, double close)
        {
            //High Low Open Close
            Double[] ymembers = new Double[4] { high, low, open, close };
            DataPoint tmp = new DataPoint(DT.ToOADate(), ymembers);
            //tmp.ToolTip = "開盤：" + open + "\n收盤：" + close + "\n最高：" + high + "\n最低：" + low;
            tmp.Tag = no;

            S_Stk.Points.Add(tmp);
        }

        void AddVolume(int no, DateTime DT, int vol)
        {
            DataPoint tmp = new DataPoint(DT.ToOADate(), vol);
            //tmp.ToolTip = "成交量：" + vol;
            S_Vol.Points.Add(tmp);
        }

        List<int> SLinePos = new List<int>();
        void AddStraightLine(int j, Color cr, double wid, bool drawLine)
        {
            StripLine stripline = new StripLine();
            stripline.Interval = 0;
            stripline.IntervalOffset = j;
            stripline.StripWidth = wid;
            stripline.BackColor = cr;
            SLinePos.Add(j - 1);
            if (drawLine) MainChart.ChartAreas["StockArea"].AxisX.StripLines.Add(stripline);
        }
        void AddFinanceLine()
        {
            //MainChart.DataManipulator.FinancialFormula(FinancialFormula.MovingAverage, "60", S_Stk, S_StkMA60);
            AlignSeries(S_Vol, S_StkMA60);

            MainChart.DataManipulator.FinancialFormula(FinancialFormula.MovingAverage, "100", S_Stk, S_StkMA);
            AlignSeries(S_Vol, S_StkMA);

            //MainChart.DataManipulator.FinancialFormula(FinancialFormula.MovingAverage, "20", S_Vol, S_VolMA20);
            AlignSeries(S_Vol, S_VolMA20);

            MainChart.DataManipulator.FinancialFormula(FinancialFormula.MovingAverageConvergenceDivergence, S_Stk, S_MACD);
            AlignSeries(S_Stk, S_MACD);

            MainChart.DataManipulator.FinancialFormula(FinancialFormula.CommodityChannelIndex, S_Stk, S_CCI); // -100 100
            AlignSeries(S_Stk, S_CCI);
            
            MainChart.DataManipulator.FinancialFormula(FinancialFormula.RelativeStrengthIndex, S_Stk, S_RSI); // 30 70
            AlignSeries(S_Stk, S_RSI);

            //for (int i = 20; i < S_Stk.Points.Count; i++)
            {
                //if (S_CCI.Points[i-1].YValues[0] >= 100 && S_RSI.Points[i-1].YValues[0] >= 70) S_Stk.Points[i].BorderColor = S_Stk.Points[i].Color = Color.Red;
                //else if (S_CCI.Points[i-1].YValues[0] <= -100 && S_RSI.Points[i-1].YValues[0] <= 30) S_Stk.Points[i].BorderColor = S_Stk.Points[i].Color = Color.LawnGreen;
                
                //if (S_CCI.Points[i].YValues[0] >= 100) S_CCI.Points[i].Color = Color.Red;
                //else if (S_CCI.Points[i].YValues[0] <= -100) S_CCI.Points[i].Color = Color.LawnGreen;
                //if (S_MACD.Points[i].YValues[0] - S_MACD.Points[i - 1].YValues[0] >= 14) S_Stk.Points[i].BorderColor = S_Stk.Points[i].Color = Color.Red;
                //else if (S_MACD.Points[i].YValues[0] - S_MACD.Points[i - 1].YValues[0] <= -14) S_Stk.Points[i].BorderColor = S_Stk.Points[i].Color = Color.LawnGreen;
                //if (S_MACDSlope.Points[i].YValues[0] > 0) S_Stk.Points[i].BorderColor = S_Stk.Points[i].Color = Color.Red;
                //else if (S_MACDSlope.Points[i].YValues[0] < 0) S_Stk.Points[i].BorderColor = S_Stk.Points[i].Color = Color.LawnGreen;
            }

            //AlignSeries(S_Stk, S_RangeTop);
            //AlignSeries(S_Stk, S_RangeBot);
            //for (int i = 0; i < S_Stk.Points.Count; i++)
            //{
            //    DataPoint tmp = new DataPoint(S_Stk.Points[i].XValue, 0);
            //    S_RangeTop.Points.Add(tmp);
            //    DataPoint tmp2 = new DataPoint(S_Stk.Points[i].XValue, 0);
            //    S_RangeBot.Points.Add(tmp2);
            //}
        }

        public static void AlignSeries(Series seriesA, Series seriesB)
        {
            var aligned = seriesA.Points.GroupJoin(seriesB.Points, a => a.XValue, b => b.XValue, (a, b) => new { a = a, b = b.SingleOrDefault() }).ToArray();
            DataPointCollection bCollection = seriesB.Points;
            bCollection.Clear();
            foreach (var pair in aligned)
            {
                DataPoint bPoint = new DataPoint();
                bPoint.XValue = pair.a.XValue;
                if (null != pair.b)
                {
                    bPoint.YValues = pair.b.YValues;
                }
                else
                {
                    bPoint.IsEmpty = true;
                }
                bCollection.Add(bPoint);
            }
        }
        #endregion

        #region Axis Setting
        private double _Offset, _VolOffset;
        private void SetAxisViewged()
        {
            int start = (int)MainChart.ChartAreas["StockArea"].AxisX.ScaleView.ViewMinimum;
            int end = (int)MainChart.ChartAreas["StockArea"].AxisX.ScaleView.ViewMaximum;

            // Series ss = chart1.Series.FindByName("SeriesName");
            // use ss instead of chart1.Series[0]

            double[] temp = S_Stk.Points.Where((x, i) => i >= start && i <= end).Select(x => x.YValues[0]).ToArray();
            //double[] temp2 = S_Stk.Points.Where((x, i) => i >= start && i <= end).Select(x => x.YValues[1]).ToArray();

            //double[] tempVol = S_Vol.Points.Where((x, i) => i >= start && i <= end).Select(x => x.YValues[0]).ToArray();

            double ymin = temp.Min() - _Offset*2;
            double ymax = temp.Max() + _Offset;

            //double yminVol = Math.Floor(tempVol.Min() * _VolOffset / 500) * 500;
            //double ymaxVol = tempVol.Max() * 1.05;

            MainChart.ChartAreas["StockArea"].AxisY.ScaleView.Position = ymin;
            MainChart.ChartAreas["StockArea"].AxisY.ScaleView.Size = ymax - ymin;
            //MainChart.ChartAreas["VolumeArea"].AxisY.ScaleView.Position = yminVol;
            //MainChart.ChartAreas["VolumeArea"].AxisY.ScaleView.Size = ymaxVol - yminVol;

            double AxisXInterval = (end - start) / 12;
            MainChart.ChartAreas["StockArea"].AxisX.Interval = AxisXInterval;
            MainChart.ChartAreas["MACDArea"].AxisX.Interval = AxisXInterval;
            MainChart.ChartAreas["CCIArea"].AxisX.Interval = AxisXInterval;
            MainChart.ChartAreas["RSIArea"].AxisX.Interval = AxisXInterval;
        }

        private void XAxisSetDateTime(bool isDay)
        {
            if (!isDay)
            {
                S_Stk.XValueType = ChartValueType.DateTime;
                S_Vol.XValueType = ChartValueType.DateTime;
                S_StkMA60.XValueType = ChartValueType.DateTime;
                S_VolMA20.XValueType = ChartValueType.DateTime;
                S_MACD.XValueType = ChartValueType.DateTime;
                S_CCI.XValueType = ChartValueType.DateTime;
                S_RSI.XValueType = ChartValueType.DateTime;
                MainChart.ChartAreas["StockArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd\nHH:mm";
                MainChart.ChartAreas["MACDArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd\nHH:mm";
                MainChart.ChartAreas["CCIArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd\nHH:mm";
                MainChart.ChartAreas["RSIArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd\nHH:mm";
            }
            else
            {
                S_Stk.XValueType = ChartValueType.Date;
                S_Vol.XValueType = ChartValueType.Date;
                S_StkMA60.XValueType = ChartValueType.Date;
                S_VolMA20.XValueType = ChartValueType.Date;
                S_MACD.XValueType = ChartValueType.Date;
                S_CCI.XValueType = ChartValueType.Date;
                S_RSI.XValueType = ChartValueType.Date;
                MainChart.ChartAreas["StockArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
                MainChart.ChartAreas["MACDArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
                MainChart.ChartAreas["CCIArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
                MainChart.ChartAreas["RSIArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
            }
        }
        //private void XAxisSetDate()
        //{
        //    S_Stk.XValueType = ChartValueType.Date;
        //    S_Vol.XValueType = ChartValueType.Date;
        //    S_StkMA60.XValueType = ChartValueType.Date;
        //    S_VolMA20.XValueType = ChartValueType.Date;
        //    S_MACD.XValueType = ChartValueType.Date;
        //    S_CCI.XValueType = ChartValueType.Date;
        //    S_RSI.XValueType = ChartValueType.Date;
        //    MainChart.ChartAreas["StockArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
        //    MainChart.ChartAreas["MACDArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
        //    MainChart.ChartAreas["CCIArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
        //    MainChart.ChartAreas["RSIArea"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
        //}
        private void ClearChart()
        {
            S_Stk.Points.Clear();
            S_Vol.Points.Clear();
            S_StkMA60.Points.Clear();
            S_VolMA20.Points.Clear();
            S_MACD.Points.Clear();
            S_RangeBot.Points.Clear();
            S_RangeTop.Points.Clear();
            S_CCI.Points.Clear();
            S_RSI.Points.Clear();

            SLinePos.Clear();
            MainChart.ChartAreas["StockArea"].AxisX.StripLines.Clear();
        }
        #endregion

        #region 雜
        private enum StrategyAction
        {
            LongBuy, LongSell, ShortBuy, ShortSell, None
        }
        private void ChartAxisViewChanged(object sender, ViewEventArgs e)
        {
            if (e.Axis.AxisName == AxisName.X)
            {
                SetAxisViewged();
            }

        }
        private void ChartAxisViewChanged(object sender, MouseEventArgs e)
        {
            SetAxisViewged();
        }
        private void btn30minClick(object sender, EventArgs e)
        {
            LoadHistoryFile(Scr.MTX30min);
        }
        private void btn1hrClick(object sender, EventArgs e)
        {
            LoadHistoryFile(Scr.MTX1hr);
        }
        private void btnDailyClick(object sender, EventArgs e)
        {
            LoadHistoryFile(Scr.MTXday);
        }

        private void btn_backtestClick(object sender, EventArgs e)
        {
            BackTest BT_window = new BackTest(odr);
            BT_window.Show();
        }
        private void btn_testClick(object sender, EventArgs e)
        {
            CurrentPrice BT_window = new CurrentPrice();
            BT_window.Show();
        }
        //int cnt = 0;
        bool isNewData = false;
        private void btn_test2Click(object sender, EventArgs e)
        {
            //isNewData = true;
            //DateTime dtTmp = DateTime.Parse("2015/04/29 09:45:00");
            //dtTmp = dtTmp.AddHours(cnt);
            //cnt++;

            //AddCandle(candle_cnt, dtTmp, 10000 - cnt * 50, 9990 - cnt * 50, 9993 - cnt * 50, 9998 - cnt * 50);
            //AddVolume(candle_cnt, dtTmp, 200);
            //candle_cnt++;
            //AddFinanceLine();
            //btn_analyzeClick(new object(), new EventArgs());
        }

        private void index_selected(object sender, EventArgs e)
        {
            int start = (int)MainChart.ChartAreas["StockArea"].AxisX.ScaleView.ViewMinimum;
            int end = (int)MainChart.ChartAreas["StockArea"].AxisX.ScaleView.ViewMaximum;

            hide_allIndex();
            MainChart.ChartAreas[((ComboBox)sender).Text + "Area"].Visible = true;
            MainChart.ChartAreas[((ComboBox)sender).Text + "Area"].AxisX.ScaleView.Position = start + 1;
            MainChart.ChartAreas[((ComboBox)sender).Text + "Area"].AxisX.ScaleView.Size = end - start - 2;
        }
        private void hide_allIndex()
        {
            MainChart.ChartAreas["VolumeArea"].Visible = false;
            MainChart.ChartAreas["MACDArea"].Visible = false;
            MainChart.ChartAreas["CCIArea"].Visible = false;
            MainChart.ChartAreas["RSIArea"].Visible = false;
        }

        //乖離率
        private double BiasRate(double pr, double ma)
        {
            return (pr - ma) / ma;
        }

        //報酬率
        private double ReturnRate(double before, double after)
        {
            return (after - before) / before;
        }

        private DateTime DaysBeforePayDay(DateTime DT)
        {
            do
            {
                DT = DT.AddDays(1);
            } while (!(((int)DT.DayOfWeek == 3 && DT.Day >= 15 && DT.Day <= 21)));
            return DT;
        }
        private bool isPayDay(int x)
        {
            foreach (int SL in SLinePos) if (SL == x) return true;
            return false;
        }
        #endregion

        private void btn_analyzeClick(object sender, EventArgs e)
        {
            N_HighLowStretegy(); //30 60min

            //Kof3Stretegy();
            //MALine_Flip();
            //N_HighLowStretegyFlip();
        }
        private void btn_analyzeClick2(object sender, EventArgs e)
        {
            WaveStretegyDay(); //day
        }

        #region UI show
        double slip = 5 + 3; //手續費+滑點
        private void ShowResultToUI()
        {
            textBox_Return.Text = total_return.ToString();
            textBox_finReturn.Text = (total_return - (total_long + total_short) * slip).ToString();
            textBox_Long.Text = total_long.ToString();
            textBox_Short.Text = total_short.ToString();
            textBox_Win.Text = total_win.ToString();
            textBox_Lose.Text = total_lose.ToString();
            textBox_Gain.Text = total_gain.ToString();
            textBox_NegGain.Text = total_NegGain.ToString();
            textBox_maxGain.Text = max_win.ToString();
            textBox_maxLose.Text = max_lose.ToString();
            textBox_empty.Text = total_empty.ToString();

            textBox_MDD.Text = max_MDD.ToString();

            total_risk = ((total_gain / total_win) / -(total_NegGain / total_lose));// -(total_lose / total_win);
            total_risk2 = ((total_gain / total_win) / -(total_NegGain / total_lose)) - (total_lose / total_win);
            textBox_risk.Text = total_risk.ToString();
            textBox_risk2.Text = total_risk2.ToString();

            textBox_avgGain.Text = (total_return / (total_win + total_lose)).ToString();

            textBox_nextaction.Text = nextstep.ToString();
        }
        private void AnalysisResultClear()
        {
            total_return = 0;
            total_win = 0;
            total_lose = 0;
            total_long = 0;
            total_short = 0;
            total_gain = 0;
            total_NegGain = 0;
            max_win = 0;
            max_lose = 0;
            total_risk = 0;
            total_risk2 = 0;
            total_MDD = 0;
            max_MDD = 0;
            total_empty = 0;

            textBox_Return.Text = "";
            textBox_finReturn.Text = "";
            textBox_Long.Text = "";
            textBox_Short.Text = "";
            textBox_Win.Text = "";
            textBox_Lose.Text = "";
            textBox_Gain.Text = "";
            textBox_NegGain.Text = "";
            textBox_maxGain.Text = "";
            textBox_maxLose.Text = "";
            textBox_risk.Text = "";
            textBox_risk2.Text = "";
            textBox_MDD.Text = "";
            textBox_empty.Text = "";

            textBox_nextaction.Text = "";

            odr.Clear();
            j = 0;
            lastI = 0;

            Flag = false;
            MaxWin = 0;
        }
        //private void reloadChart()
        //{
        //    ClearChart();
        //    LoadHistoryFileDaily();
        //}

        private void SetPointLable(DataPoint pt, Color bg)
        {
            pt.LabelForeColor = Color.Wheat;
            pt.LabelBackColor = bg;
            pt.Label = pt.YValues[2].ToString();
            pt.IsValueShownAsLabel = true;
        }
        #endregion

        #region Build Cover
        int lastI;
        private void BuildLong(int i)
        {
            OrderPosition _odr = new OrderPosition(DateTime.FromOADate(S_Stk.Points[i].XValue),
                        S_Stk.Points[i].YValues[2], OrderPosition.Type.Long);

            //AddStraightLine(i+1, Color.Red, 0.1);
            
            total_long++;
            odr.Add(_odr);

            SetPointLable(S_Stk.Points[i], Color.Red);

            total_empty += i - lastI;
        }
        private void CoverLong(int i)
        {
            double _return = odr[j].CoverPosition(DateTime.FromOADate(S_Stk.Points[i].XValue), S_Stk.Points[i].YValues[2]);
            total_return += _return;
            //AddStraightLine(i + 1, Color.DarkRed, 0.1);

            if (_return > 0)
            {
                total_win++;
                if (_return >= max_win) max_win = _return;
                total_gain += _return;
                S_Stk.Points[i].LabelBorderColor = Color.Gold;

                if (isDD)
                {
                    isDD = false;
                    if (total_MDD < max_MDD) max_MDD = total_MDD;
                    total_MDD = 0;
                }
            }
            else
            {
                total_NegGain += _return;
                if (_return <= max_lose) max_lose = _return;
                total_lose++;

                if (!isDD) isDD = true;
                else total_MDD += _return;
            }

            j++;

            SetPointLable(S_Stk.Points[i], Color.DarkRed);
            S_Stk.Points[i].Label += "\n" + _return;

            lastI = i;
        }
        private void BuildShort(int i)
        {
            OrderPosition _odr = new OrderPosition(DateTime.FromOADate(S_Stk.Points[i].XValue),
                        S_Stk.Points[i].YValues[2], OrderPosition.Type.Short);

            //AddStraightLine(i+1, Color.LawnGreen, 0.1);

            total_short++;
            odr.Add(_odr);

            SetPointLable(S_Stk.Points[i], Color.Green);

            total_empty += i - lastI;
        }
        private void CoverShort(int i)
        {
            double _return = odr[j].CoverPosition(DateTime.FromOADate(S_Stk.Points[i].XValue), S_Stk.Points[i].YValues[2]);
            total_return += _return;
            //AddStraightLine(i+1, Color.DarkGreen, 0.1);

            if (_return > 0)
            {
                total_win++;
                if (_return >= max_win) max_win = _return;
                total_gain += _return;
                S_Stk.Points[i].LabelBorderColor = Color.Gold;

                if (isDD)
                {
                    isDD = false;
                    if (total_MDD < max_MDD) max_MDD = total_MDD;
                    total_MDD = 0;
                }
            }
            else
            {
                total_NegGain += _return;
                if (_return <= max_lose) max_lose = _return;

                if (!isDD) isDD = true;
                else total_MDD += _return;

                total_lose++;
            }

            j++;

            SetPointLable(S_Stk.Points[i], Color.DarkGreen);
            S_Stk.Points[i].Label += "\n" + _return;

            lastI = i;
        }
        #endregion

        //策略
        private List<OrderPosition> odr = new List<OrderPosition>();
        private int j = 0;
        private double total_return, total_win, total_lose, total_long, total_short, max_win, max_lose, total_risk, total_risk2;
        private double total_gain, total_NegGain;
        private double max_MDD, total_MDD, total_empty;
        private bool isDD = false;

        private double MaxWin = 0;
        private bool Flag = false;
        StrategyAction nextstep = StrategyAction.None;

        //自動下單

        #region Note
        //指標根本 平均線
        //先做單邊
        //不要求淨利最高的參數，要找每年績效穩定的參數
        //順勢系統 趨勢出現才進場 不會在最低點買 最高點賣
        //順勢 趨向指標(DMI)、動能指標(MTM)
        //逆勢系統為出發點，則可考慮以RSI、KD

        //RSI停利 波動率 http://wenschair.blogspot.tw/2013/09/18_17.html
        #endregion

        //======OLD=======================固定比率停利停損==============================
        #region ConstPercentageStopStretegy()
        private void ConstPercentageStopStretegy()
        {
            AnalysisResultClear();
            double yesd_close;
            double yesd_ma;
            double yesd_bias;

            double stopWin = 0.06;
            double stopLose = -0.03;
            bool flag = false;

            for(int i=60;i<S_Stk.Points.Count;i++)
            {
                yesd_close=S_Stk.Points[i - 1].YValues[3];
                yesd_ma=S_StkMA60.Points[i - 1].YValues[0];

                yesd_bias = BiasRate(yesd_close, yesd_ma);

                //多單
                if (!flag && yesd_bias < 0.02 && yesd_bias > 0)
                {
                    OrderPosition _odr = new OrderPosition(DateTime.FromOADate(S_Stk.Points[i].XValue),
                        S_Stk.Points[i].YValues[2], OrderPosition.Type.Long);

                    //AddStraightLine(i, Color.Red, 0.1);
                    flag = true;
                    total_long++;
                    odr.Add(_odr);
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long &&
                    (ReturnRate(odr[j].openPrice, yesd_close) > stopWin || ReturnRate(odr[j].openPrice, yesd_close) < stopLose))
                {
                    double _return = odr[j].CoverPosition(DateTime.FromOADate(S_Stk.Points[i].XValue), S_Stk.Points[i].YValues[2]);
                    total_return += _return;
                    //AddStraightLine(i, Color.DarkRed, 0.1);

                    flag = false;
                    if (_return > 0)
                    {
                        total_win++;
                        total_gain += _return;
                    }
                    else
                    {
                        total_NegGain += _return;
                        total_lose++;
                    }

                    j++;
                }
                //空單
                else if (!flag && yesd_bias > -0.02 && yesd_bias < 0)
                {
                    OrderPosition _odr = new OrderPosition(DateTime.FromOADate(S_Stk.Points[i].XValue),
                        S_Stk.Points[i].YValues[2], OrderPosition.Type.Short);

                    //AddStraightLine(i, Color.LawnGreen, 0.1);
                    flag = true;
                    total_short++;
                    odr.Add(_odr);
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Short &&
                    (ReturnRate(yesd_close, odr[j].openPrice) > stopWin || ReturnRate(yesd_close, odr[j].openPrice) < stopLose))
                {
                    double _return = odr[j].CoverPosition(DateTime.FromOADate(S_Stk.Points[i].XValue), S_Stk.Points[i].YValues[2]);
                    total_return += _return;
                    //AddStraightLine(i, Color.GreenYellow, 0.1);

                    flag = false;
                    if (_return > 0)
                    {
                        total_win++;
                        total_gain += _return;
                    }
                    else
                    {
                        total_NegGain += _return;
                        total_lose++;
                    }

                    j++;
                }
            }
            ShowResultToUI();
        }
        #endregion
        //======OLD=======================固定比率停利停損==============================

        //=======XX======================比率回檔移動停利停損(百分比移動停利法)============此為逆勢系統==================
        #region RangePercentageStopStretegy()
        private void RangePercentageStopStretegy()
        {
            AnalysisResultClear();
            double yesd_close;
            double yesd_ma;
            //double yesd_bias;

            double stopRate = 0.55;
            double stopLoseRate = -0.02;
            //double EnterbiasRate = 0.02;
            double HighP = 0;
            double LowP = 0;

            bool flag = false;

            for (int i = 31; i < S_Stk.Points.Count; i++)
            {
                yesd_close = S_Stk.Points[i - 1].YValues[3];
                yesd_ma = S_StkMA60.Points[i - 1].YValues[0];
                //yesd_bias = BiasRate(yesd_close, yesd_ma);

                //多單
                //if (!flag && yesd_bias <= EnterbiasRate && yesd_bias > 0) //Bias介於一定值
                if (!flag && S_CCI.Points[i-1].YValues[0] >= 90 && S_RSI.Points[i-1].YValues[0] >= 60)
                {
                    BuildLong(i);

                    flag = true;
                    HighP = S_Stk.Points[i].YValues[2];

                    S_RangeBot.Points[i].YValues[0] = odr[j].openPrice + (HighP - odr[j].openPrice) * stopRate;
                    S_RangeBot.Points[i - 1].YValues[0] = S_RangeBot.Points[i].YValues[0];
                    S_RangeBot.Points[i].Color = Color.Purple;
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long)
                {
                    double botLimit = odr[j].openPrice + (HighP - odr[j].openPrice) * stopRate;
                    double Now_returnR = ReturnRate(odr[j].openPrice, S_Stk.Points[i - 1].YValues[3]);

                    S_RangeBot.Points[i].YValues[0] = botLimit;
                    S_RangeBot.Points[i].Color = Color.Purple;

                    if ((HighP != odr[j].openPrice && botLimit > S_Stk.Points[i - 1].YValues[3]) || Now_returnR <= stopLoseRate )
                    {
                        CoverLong(i);
                        flag = false;
                    }
                    else if (S_Stk.Points[i].YValues[0] > HighP) HighP = S_Stk.Points[i].YValues[0];
                }
                //空單
                //else if (!flag && yesd_bias >= -EnterbiasRate && yesd_bias < 0)
                //else if (!flag && S_CCI.Points[i-1].YValues[0] <= -90 && S_RSI.Points[i-1].YValues[0] <= 40)
                //{
                //    BuildShort(i);
                    
                //    flag = true;
                //    LowP = S_Stk.Points[i].YValues[2];
                //    S_RangeTop.Points[i].YValues[0] = odr[j].openPrice - (odr[j].openPrice - LowP) * stopRate;
                //    S_RangeTop.Points[i - 1].YValues[0] = S_RangeTop.Points[i].YValues[0];
                //    S_RangeTop.Points[i].Color = Color.Violet;
                //}
                //else if (flag && odr[j].OrderType == OrderPosition.Type.Short)
                //{
                //    double topLimit = odr[j].openPrice - (odr[j].openPrice - LowP) * stopRate;
                //    double Now_returnR = ReturnRate(S_Stk.Points[i - 1].YValues[3], odr[j].openPrice);

                //    S_RangeTop.Points[i].YValues[0] = topLimit;
                //    S_RangeTop.Points[i].Color = Color.Violet;

                //    if ((LowP != odr[j].openPrice && topLimit < S_Stk.Points[i - 1].YValues[3]) || Now_returnR <= stopLoseRate)
                //    {
                //        CoverShort(i);
                //        flag = false;
                //    }
                //    else if (S_Stk.Points[i].YValues[1] < LowP) LowP = S_Stk.Points[i].YValues[1];
                    
                //}
            }
            ShowResultToUI();
        }
        #endregion
        //=======XX======================比率回檔移動停利停損(百分比移動停利法)==============================

        //===========OK==================破N根K棒高點或低點進場法=========30min 60min only=====================
        //使用參數:停損(*2),破N根
        //http://www.bituzi.com/2013/09/PriceChannel.html
        #region
        //10	12509	7166	5.0343	3274	15.236	-981
        //11	13144	7872.5	5.0223	3447	16.227	-981
        //13	14763	9732	5.3719	3671	19.098	-1003
        //14	14060	9048.5	5.4294	3905	18.259	-1003
        //15	13776	8699	5.5733	4065	17.661	-1003
        //16	13171	8218	5.2942	4241	17.307	-1076
        //17	14212	9298	5.4078	4425	18.823	-1076
        //18	13366	8426	5.314	4679	17.61	-1076
        //20	12371	7600	5.1919	4920	16.877	-1048
        #endregion
        #region N_HighLowStretegy()
        private void N_HighLowStretegy()
        {
            AnalysisResultClear();

            double stopLoseRate = -0.02;
            double stopLosePoint = -100;
            //double stopWinPoint = 20;
            //double maxWin = 0;
            //bool flag = false;

            List<double> L_RangeTop = new List<double>();
            List<double> L_RangeBot = new List<double>();

            int N = 15;
            for (int i = 1; i < S_Stk.Points.Count; i++)
            {
                if (L_RangeTop.Count < N)
                {
                    L_RangeTop.Add(S_Stk.Points[i - 1].YValues[0]);
                    L_RangeBot.Add(S_Stk.Points[i - 1].YValues[1]);
                    continue;
                }

                //S_RangeTop.Points[i].XValue = S_Stk.Points[i].XValue;
                //S_RangeBot.Points[i].XValue = S_Stk.Points[i].XValue;
                //S_RangeTop.Points[i].YValues[0] = L_RangeTop.Max();
                //S_RangeBot.Points[i].YValues[0] = L_RangeBot.Min();
                //S_RangeTop.Points[i].Color = Color.Yellow;
                //S_RangeBot.Points[i].Color = Color.Yellow;

                //多單
                if (!Flag && !isPayDay(i) && S_Stk.Points[i - 1].YValues[0] > L_RangeTop.Max())
                {
                    BuildLong(i);
                    if (isNewData && i == S_Stk.Points.Count - 1) ;

                    Flag = true;
                    MaxWin = 0;
                }
                else if (Flag && odr[j].OrderType == OrderPosition.Type.Long)
                {
                    double Now_returnR = ReturnRate(odr[j].openPrice, S_Stk.Points[i - 1].YValues[3]);
                    double Now_returnV = S_Stk.Points[i - 1].YValues[3] - odr[j].openPrice;

                    if (Now_returnV > MaxWin) MaxWin = Now_returnV;

                    if (Now_returnR <= stopLoseRate || Now_returnV <= stopLosePoint || isPayDay(i)
                        || (MaxWin > 0 && Now_returnV < 0)
                        )
                    {
                        CoverLong(i);
                        if (isNewData && i == S_Stk.Points.Count - 1) ;

                        Flag = false;
                    }

                }
                //空單
                else if (!Flag && !isPayDay(i) && S_Stk.Points[i - 1].YValues[1] < L_RangeBot.Min())
                {
                    BuildShort(i);
                    if (isNewData && i == S_Stk.Points.Count - 1) ;

                    Flag = true;
                    MaxWin = 0;
                }
                else if (Flag && odr[j].OrderType == OrderPosition.Type.Short)
                {
                    double Now_returnR = ReturnRate(S_Stk.Points[i - 1].YValues[3], odr[j].openPrice);
                    double Now_returnV = odr[j].openPrice - S_Stk.Points[i - 1].YValues[3];

                    if (Now_returnV > MaxWin) MaxWin = Now_returnV;

                    if (Now_returnR <= stopLoseRate || Now_returnV <= stopLosePoint || isPayDay(i)
                        || (MaxWin > 0 && Now_returnV < 0)
                        )
                    {
                        CoverShort(i);
                        if (isNewData && i == S_Stk.Points.Count - 1) ;

                        Flag = false;
                    }

                }

                L_RangeTop.Add(S_Stk.Points[i - 1].YValues[0]);
                L_RangeBot.Add(S_Stk.Points[i - 1].YValues[1]);
                if (L_RangeTop.Count == N + 1)
                {
                    L_RangeTop.RemoveAt(0);
                    L_RangeBot.RemoveAt(0);
                }
            }
            ShowResultToUI();
        }
        #endregion
        //=============================破N根K棒高點或低點進場法==============================

        //==========OK===================RSI順勢 for day==============================
        //使用參數:RSI差值, 停利停損, 天數趨勢
        #region result
        //Result
        //0.5	9080	7006.5	1.6882	323
        //1	9530	7456.5	1.7577	334
        //2	10842	8794.5	1.8161	354
        //3	11810	9775.5	1.8579	384

        //4	12317	10302	1.8976	411
        //5	14967	13023	1.9022	449
        //6	14988	13083	1.8792	519
        //7	14063	12178	1.8426	584
        //8	16694	14900	1.9496	674
        //9	15696	13960	1.8959	789
        //10	16994	15317	1.9745	897

        //11	19616	18062	2.1971	1103
        //12	19872	18416	2.2778	1258
        //13	19162	17790	2.3523	1399
        //14	18358	17077	2.2833	1516
        //15	19125	17961	2.2397	1729
        //16	19252	18186	2.2348	1931
        #endregion
        #region WaveStretegyDay()
        private void WaveStretegyDay()
        {
            AnalysisResultClear();

            //double stopLoseRate = -0.025;
            //double stopLosePoint = -150;
            //double stopWinPoint = 150;
            //double stopWinR = 0.5;

            //double macd_m = 11; //8
            for (int i = 100; i < S_Stk.Points.Count; i++)
            {
                StrategyAction res = RSIStretegyInOut(i);

                if (res == StrategyAction.LongBuy) BuildLong(i);
                else if (res == StrategyAction.LongSell) CoverLong(i);
                else if (res == StrategyAction.ShortBuy) BuildShort(i);
                else if (res == StrategyAction.ShortSell) CoverShort(i);

            }
            nextstep = RSIStretegyInOut(S_Stk.Points.Count);
            ShowResultToUI();
        }
        private StrategyAction RSIStretegyInOut(int i)
        {
            //double stopLoseRate = -0.025;
            //double stopLosePoint = -100;
            //double stopWinPoint = 150;
            //double stopWinR = 0.5;
            //double rsi_m = 18;

            //if (!flag && S_MACD.Points[i].YValues[0] - S_MACD.Points[i - 1].YValues[0] >= 0 && Math.Abs(S_MACD.Points[i].YValues[0]) > 20)
            //if (!Flag && S_MACD.Points[i - 1].YValues[0] - S_MACD.Points[i - 2].YValues[0] >= macd_m)
            if (!Flag && S_StkMA.Points[i - 1].YValues[0] < S_Stk.Points[i - 1].YValues[3]
                )
            {
                Flag = true;
                MaxWin = 0;
                return StrategyAction.LongBuy;
            }
            else if (Flag && odr[j].OrderType == OrderPosition.Type.Long)
            {
                double Now_returnR = ReturnRate(odr[j].openPrice, S_Stk.Points[i - 1].YValues[3]);
                double Now_returnV = S_Stk.Points[i - 1].YValues[3] - odr[j].openPrice;

                if (Now_returnV > MaxWin) MaxWin = Now_returnV;

                //if ((MaxWin - Now_returnV >= stopWinPoint && MaxWin > 0) || Now_returnV < stopLosePoint)
                if (S_StkMA.Points[i - 1].YValues[0] > S_Stk.Points[i - 1].YValues[3])
                {
                    Flag = false;
                    return StrategyAction.LongSell;
                }

            }
            //空單
            //else if (!flag && S_MACD.Points[i].YValues[0] - S_MACD.Points[i - 1].YValues[0] <= 0 && Math.Abs(S_MACD.Points[i].YValues[0]) > 20)
            //else if (!Flag && S_MACD.Points[i - 1].YValues[0] - S_MACD.Points[i - 2].YValues[0] <= -macd_m)
            else if (!Flag && S_StkMA.Points[i - 1].YValues[0] > S_Stk.Points[i - 1].YValues[3]
                )
            {
                Flag = true;
                MaxWin = 0;
                return StrategyAction.ShortBuy;
            }
            else if (Flag && odr[j].OrderType == OrderPosition.Type.Short)
            {
                double Now_returnR = ReturnRate(S_Stk.Points[i - 1].YValues[3], odr[j].openPrice);
                double Now_returnV = odr[j].openPrice - S_Stk.Points[i - 1].YValues[3];

                if (Now_returnV > MaxWin) MaxWin = Now_returnV;

                //if ((MaxWin - Now_returnV >= stopWinPoint && MaxWin > 0) || Now_returnV < stopLosePoint)
                if (S_StkMA.Points[i - 1].YValues[0] < S_Stk.Points[i - 1].YValues[3])
                {
                    Flag = false;
                    return StrategyAction.ShortSell;
                }

            }
            return StrategyAction.None;
        }
        #endregion
        //=============================RSI順勢 for day==============================

        //=============================突破均線翻單===============30min 60min only==============
        #region MALine_Flip()
        private void MALine_Flip()
        {
            AnalysisResultClear();

            bool flag = false;
            //double stopLoseRate = -0.02;
            double maxWin = 0;
            double stopWinPnt = 200;
            double flipPointR = 0.5;

            for (int i = 63; i < S_Stk.Points.Count; i++)
            {
                //多單
                if (flag && odr[j].OrderType == OrderPosition.Type.Short
                    && S_Stk.Points[i - 1].YValues[3] > S_StkMA.Points[i - 1].YValues[0])
                {
                    CoverShort(i);
                    BuildLong(i);
                    maxWin = 0;
                    //flag = true;

                }
                else if (!flag && S_Stk.Points[i - 1].YValues[3] > S_StkMA.Points[i - 1].YValues[0])
                {
                    BuildLong(i);
                    flag = true;
                }
                //else if (flag && odr[j].OrderType == OrderPosition.Type.Long
                //    && S_Stk.Points[i - 1].YValues[3] > S_StkMA60.Points[i - 1].YValues[0])
                //{
                //    double Now_returnR = ReturnRate(odr[j].openPrice, S_Stk.Points[i - 1].YValues[3]);
                //    double Now_returnV = S_Stk.Points[i - 1].YValues[3] - odr[j].openPrice;

                //    if (Now_returnV > maxWin) maxWin = Now_returnV;
                //    //if (Now_returnR <= stopLoseRate || Now_returnV <= -100)
                //    //{
                //    //    CoverLong(i);
                //    //    BuildShort(i);
                //    //    //flag = false;
                //    //}
                //    if ((maxWin - Now_returnV > stopWinPnt /*|| maxWin * flipPointR > Now_returnV*/) && maxWin > 0)
                //    {
                //        CoverLong(i);
                //        BuildShort(i);
                //        maxWin = 0;
                //    }
                //}
                //空單
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long
                    && S_Stk.Points[i - 1].YValues[3] < S_StkMA.Points[i - 1].YValues[0])
                {
                    CoverLong(i);
                    BuildShort(i);
                    maxWin = 0;
                    //flag = true;
                }
                else if (!flag && S_Stk.Points[i - 1].YValues[3] < S_StkMA.Points[i - 1].YValues[0])
                {
                    BuildShort(i);
                    flag = true;
                }
                //else if (flag && odr[j].OrderType == OrderPosition.Type.Short
                //    && S_Stk.Points[i - 1].YValues[3] < S_StkMA60.Points[i - 1].YValues[0])
                //{
                //    double Now_returnR = ReturnRate(S_Stk.Points[i - 1].YValues[3], odr[j].openPrice);
                //    double Now_returnV = odr[j].openPrice - S_Stk.Points[i - 1].YValues[3];

                //    if (Now_returnV > maxWin) maxWin = Now_returnV;
                //    //if (Now_returnR <= stopLoseRate || Now_returnV <= -100)
                //    //{
                //    //    CoverShort(i);
                //    //    BuildLong(i);
                //    //    //flag = false;
                //    //}
                //    if ((maxWin - Now_returnV > stopWinPnt /*|| maxWin * flipPointR > Now_returnV*/) && maxWin > 0)
                //    {
                //        CoverShort(i);
                //        BuildLong(i);
                //        maxWin = 0;
                //    }
                //}
            }
            ShowResultToUI();
        }
        #endregion
        //=============================突破均線翻單=============================

        //=============================The Vortex indicator=============================
        //http://wenschair.blogspot.tw/2014/10/the-vortex-indicator.html

        //=============================The Vortex indicator=============================

        //===========skip==================破N根K棒高點或低點進場法+翻單==============================
        //使用參數:停損(*2),破N根
        //http://www.bituzi.com/2013/09/PriceChannel.html
        #region N_HighLowStretegyFlip()
        private void N_HighLowStretegyFlip()
        {
            AnalysisResultClear();

            //double stopLoseRate = -0.02;
            //double stopLosePoint = -100;
            //double stopWinPoint = 20;
            double maxWin = 0;
            bool flag = false;

            List<double> L_RangeTop = new List<double>();
            List<double> L_RangeBot = new List<double>();

            int N = 15;
            for (int i = 1; i < S_Stk.Points.Count; i++)
            {
                if (L_RangeTop.Count < N)
                {
                    L_RangeTop.Add(S_Stk.Points[i - 1].YValues[0]);
                    L_RangeBot.Add(S_Stk.Points[i - 1].YValues[1]);
                    continue;
                }

                //多單
                if (!flag && S_Stk.Points[i - 1].YValues[0] > L_RangeTop.Max())
                {
                    BuildLong(i);

                    flag = true;
                    maxWin = 0;
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long && S_Stk.Points[i - 1].YValues[3] > L_RangeBot.Min())
                {
                    double Now_returnR = ReturnRate(odr[j].openPrice, S_Stk.Points[i - 1].YValues[3]);
                    double Now_returnV = S_Stk.Points[i - 1].YValues[3] - odr[j].openPrice;

                    if (Now_returnV > maxWin) maxWin = Now_returnV;

                    if (maxWin > 0 && Now_returnV < 0)
                    {
                        CoverLong(i);
                        BuildShort(i);
                        maxWin = 0;

                    }

                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long && S_Stk.Points[i - 1].YValues[3] < L_RangeBot.Min())
                {
                    CoverLong(i);
                    BuildShort(i);
                    maxWin = 0;
                }

                //空單
                else if (!flag && S_Stk.Points[i - 1].YValues[1] < L_RangeBot.Min())
                {
                    BuildShort(i);
                    flag = true;
                    maxWin = 0;
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Short && S_Stk.Points[i - 1].YValues[3] > L_RangeTop.Max())
                {
                    CoverShort(i);
                    BuildLong(i);
                    maxWin = 0;
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Short && S_Stk.Points[i - 1].YValues[3] < L_RangeTop.Max())
                {
                    double Now_returnR = ReturnRate(S_Stk.Points[i - 1].YValues[3], odr[j].openPrice);
                    double Now_returnV = odr[j].openPrice - S_Stk.Points[i - 1].YValues[3];

                    if (Now_returnV > maxWin) maxWin = Now_returnV;

                    if (maxWin > 0 && Now_returnV < 0)
                    {
                        CoverShort(i);
                        BuildLong(i);
                        maxWin = 0;
                    }

                }

                L_RangeTop.Add(S_Stk.Points[i - 1].YValues[0]);
                L_RangeBot.Add(S_Stk.Points[i - 1].YValues[1]);
                if (L_RangeTop.Count == N + 1)
                {
                    L_RangeTop.RemoveAt(0);
                    L_RangeBot.RemoveAt(0);
                }
            }
            ShowResultToUI();
        }
        #endregion
        //=============================破N根K棒高點或低點進場法==============================

        //=============================三根K線=========只能多單=====================
        //http://trading16888.pixnet.net/blog/post/116229859
        #region Kof3Stretegy()
        private void Kof3Stretegy()
        {
            AnalysisResultClear();

            bool flag = false;
            double stopLoseRate = -0.02;

            for (int i = 5; i < S_Stk.Points.Count; i++)
            {

                //多單
                if (!flag && S_Stk.Points[i - 3].YValues[3] > S_Stk.Points[i - 4].YValues[3]
                    && S_Stk.Points[i - 2].YValues[0] > S_Stk.Points[i - 3].YValues[0]
                    && S_Stk.Points[i - 2].YValues[1] < S_Stk.Points[i - 3].YValues[1]
                    && S_Stk.Points[i - 1].YValues[3] > S_Stk.Points[i - 2].YValues[3])
                {
                    BuildLong(i);

                    flag = true;

                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long)
                {
                    double Now_returnR = ReturnRate(odr[j].openPrice, S_Stk.Points[i - 1].YValues[3]);
                    double Now_returnV = S_Stk.Points[i - 1].YValues[3] - odr[j].openPrice;
                    if (Now_returnR <= stopLoseRate || Now_returnV <= -100 || isPayDay(i))
                    {
                        CoverLong(i);
                        flag = false;
                    }

                }
                //空單
                //else if (!flag && S_Stk.Points[i - 3].YValues[3] < S_Stk.Points[i - 4].YValues[3]
                //    && S_Stk.Points[i - 2].YValues[0] < S_Stk.Points[i - 3].YValues[0]
                //    && S_Stk.Points[i - 2].YValues[1] > S_Stk.Points[i - 3].YValues[1]
                //    && S_Stk.Points[i - 1].YValues[3] < S_Stk.Points[i - 2].YValues[3])
                //{
                //    BuildShort(i);
                //    flag = true;
                //}
                //else if (flag && odr[j].OrderType == OrderPosition.Type.Short)
                //{
                //    double Now_returnR = ReturnRate(S_Stk.Points[i - 1].YValues[3], odr[j].openPrice);
                //    double Now_returnV = odr[j].openPrice - S_Stk.Points[i - 1].YValues[3];

                //    if (Now_returnR <= stopLoseRate || Now_returnV <= -100 || isPayDay(i))
                //    {
                //        CoverShort(i);
                //        flag = false;
                //    }

                //}
            }
            ShowResultToUI();
        }
        #endregion
        //=============================三根K線==============================

        //====XX=========================K棒階梯移動停利法========進場:突破均線======================
        //http://rane1220.pixnet.net/blog/post/65284525-%E6%88%91%E5%B8%B8%E7%94%A8%E7%9A%84%E5%81%9C%E5%88%A9%E5%87%BA%E5%A0%B4%E6%96%B9%E6%B3%95(%E4%BA%8C)
        #region StairMovingStretegy()
        private void StairMovingStretegy()
        {
            AnalysisResultClear();

            double yesd2_close;
            double yesd_close;
            double yesd2_ma;
            double yesd_ma;
            bool flag = false;

            double stair_top = 0;
            double stair_bot = 0;

            for (int i = 60; i < S_Stk.Points.Count; i++)
            {
                yesd2_close = S_Stk.Points[i - 2].YValues[3];
                yesd_close = S_Stk.Points[i - 1].YValues[3];
                yesd2_ma = S_StkMA60.Points[i - 2].YValues[0];
                yesd_ma = S_StkMA60.Points[i - 1].YValues[0];

                bool tmpbool = flag;

                //多單
                if (!flag && yesd2_close < yesd2_ma && yesd_close > yesd_ma) //進場:突破季線
                //if (!flag && S_Stk.Points[i - 1].YValues[3] > S_RangeTop.Points[i - 1].YValues[0]) //進場:突破區間
                {
                    OrderPosition _odr = new OrderPosition(DateTime.FromOADate(S_Stk.Points[i].XValue),
                        S_Stk.Points[i].YValues[2], OrderPosition.Type.Long);

                    stair_top = Math.Max(S_Stk.Points[i - 1].YValues[0], S_Stk.Points[i].YValues[0]) + 20;
                    stair_bot = Math.Min(S_Stk.Points[i - 1].YValues[1], S_Stk.Points[i].YValues[1]) - 20;

                    //AddStraightLine(i+1, Color.Red, 0.1);
                    flag = true;
                    total_long++;
                    odr.Add(_odr);
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long &&
                     S_Stk.Points[i - 1].YValues[1] < stair_bot) //出場:階梯
                {
                    double _return = odr[j].CoverPosition(DateTime.FromOADate(S_Stk.Points[i].XValue), S_Stk.Points[i].YValues[2]);
                    total_return += _return;
                    //AddStraightLine(i+1, Color.DarkRed, 0.1);

                    flag = false;
                    if (_return > 0)
                    {
                        total_win++;
                        total_gain += _return;
                    }
                    else
                    {
                        total_NegGain += _return;
                        total_lose++;
                    }

                    j++;
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Long &&
                    S_Stk.Points[i].YValues[0] > stair_top)
                {
                    stair_top = S_Stk.Points[i].YValues[0];
                    stair_bot = Math.Min(S_Stk.Points[i - 1].YValues[1], S_Stk.Points[i].YValues[1]);
                }
                //空單
                else if (!flag && yesd2_close > yesd2_ma && yesd_close < yesd_ma)
                //else if (!flag && S_Stk.Points[i - 1].YValues[3] < S_RangeBot.Points[i - 1].YValues[0])
                {
                    OrderPosition _odr = new OrderPosition(DateTime.FromOADate(S_Stk.Points[i].XValue),
                        S_Stk.Points[i].YValues[2], OrderPosition.Type.Short);

                    stair_top = Math.Max(S_Stk.Points[i - 1].YValues[0], S_Stk.Points[i].YValues[0]) + 20;
                    stair_bot = Math.Min(S_Stk.Points[i - 1].YValues[1], S_Stk.Points[i].YValues[1]) - 20;

                    //AddStraightLine(i+1, Color.LawnGreen, 0.1);
                    flag = true;
                    total_short++;
                    odr.Add(_odr);
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Short &&
                     S_Stk.Points[i-1].YValues[0] > stair_top)
                {
                    double _return = odr[j].CoverPosition(DateTime.FromOADate(S_Stk.Points[i].XValue), S_Stk.Points[i].YValues[2]);
                    total_return += _return;
                    //AddStraightLine(i+1, Color.GreenYellow, 0.1);

                    flag = false;
                    if (_return > 0)
                    {
                        total_win++;
                        total_gain += _return;
                    }
                    else
                    {
                        total_NegGain += _return;
                        total_lose++;
                    }

                    j++;
                }
                else if (flag && odr[j].OrderType == OrderPosition.Type.Short &&
                    S_Stk.Points[i].YValues[1] < stair_bot)
                {
                    stair_top = Math.Max(S_Stk.Points[i - 1].YValues[0], S_Stk.Points[i].YValues[0]);
                    stair_bot = S_Stk.Points[i].YValues[1];
                }


                int a = j;
                if (flag == false && tmpbool == true)
                {
                    a--;
                }
                //DataPoint tmp = new DataPoint(S_Stk.Points[i].XValue, stair_top);
                //DataPoint tmp2 = new DataPoint(S_Stk.Points[i].XValue, stair_bot);
                //if (tmpbool)
                //{
                //    if (odr[a].OrderType == OrderPosition.Type.Long)
                //    {
                //        S_RangeBot.Points[S_RangeBot.Points.Count - 1].YValues[0] = stair_bot;
                //        tmp.Color = Color.Transparent;
                //    }
                //    else if (odr[a].OrderType == OrderPosition.Type.Short)
                //    {
                //        S_RangeTop.Points[S_RangeTop.Points.Count - 1].YValues[0] = stair_top;
                //        tmp2.Color = Color.Transparent;
                //    }
                //}
                //else
                //{
                //    tmp.Color = Color.Transparent;
                //    tmp2.Color = Color.Transparent;
                //}
                //S_RangeTop.Points.Add(tmp);
                //S_RangeBot.Points.Add(tmp2);
            }
            ShowResultToUI();
        }
        #endregion
        //====XX=========================K棒階梯移動停利法==============================

    }

    public class OrderPosition
    {
        public OrderPosition(DateTime _DT, double _atPrice, Type _ty)
        {
            openPrice = _atPrice;
            OrderType = _ty;
            OpenDT = _DT;
        }

        public double CoverPosition(DateTime _DT, double price)
        {
            CloseDT = _DT;
            closePrice = price;
            isCovered = true;
            if (OrderType == Type.Long) returnValue = closePrice - openPrice;
            else returnValue = openPrice - closePrice;
            return returnValue;
        }

        public double openPrice, closePrice, returnValue;
        public Type OrderType;
        public DateTime OpenDT, CloseDT;
        public bool isCovered = false;
        public enum Type
        {
            Long,
            Short
        }
    }

    /*public class CandleObj
    {
        public int no;
        public DateTime DT;
        public Double[] ymembers;

        public CandleObj(int _no, DateTime _DT, Double[] _ymem)
        {
            no = _no;
            DT = _DT;
            ymembers = _ymem;
        }

    }*/
}
