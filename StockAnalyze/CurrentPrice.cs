﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockAnalyze
{
    public partial class CurrentPrice : Form
    {
        Timer tm = new Timer();
        
        public CurrentPrice()
        {
            InitializeComponent();

            tm.Interval = 20;
            tm.Tick += tm_Tick;
            tm.Start();
        }

        #region variables
        DateTime dtNow = DateTime.Now;
        static string targetID = "MXFE5";
        static string fileName = @"min_tick.log";
        static string sourcePath = @"C:\Primasia\log";
        static string targetPath = @"C:\Primasia\log\bak";
        string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
        string destFile = System.IO.Path.Combine(targetPath, fileName);

        DateTime _start;
        DateTime _end;
        bool isReady = false;
        #endregion

        void tm_Tick(object sender, EventArgs e)
        {
            dtNow = DateTime.Now;

            if (!isReady && dtNow.Minute == 44 && dtNow.Second == 40 && dtNow.Hour > 8 && dtNow.Hour <= 13)
            {
                System.IO.File.Copy(sourceFile, destFile, true);

                string[] lines = System.IO.File.ReadAllLines(destFile);

                for (int i = 0; i < lines.Length; i++)
                {
                    string[] res = lines[i].Split(' ');
                    if (res.Length < 12) continue;
                    if (res[2] != targetID + ",") continue;

                    _start = DateTime.Parse(res[0]).AddHours(dtNow.Hour - 9);
                    _end = _start.AddHours(1);
                    if (dtNow.Hour == 13) _end = _end.AddSeconds(1);

                    break;
                }
                isReady = true;
            }

            if (isReady && dtNow >= _end)
            {
                tm.Stop();

                System.IO.File.Copy(sourceFile, destFile, true);

                //System.IO.StreamWriter file = new System.IO.StreamWriter("hr_detail.log",true);
                System.IO.StreamWriter file2 = new System.IO.StreamWriter("hr.log",true);
                string[] lines = System.IO.File.ReadAllLines(destFile);
                string strlog = "";

                int j = 0;

                double _high = 0, _low = 999999, _open = 0, _close = 0, _vol = 0;
                double _price = 0;

                for (int i = lines.Length - 1; i >= 0; i--)
                {
                    string[] res = lines[i].Split(' ');
                    if (res.Length < 12) continue;
                    if (res[2] != targetID + ",") continue;

                    DateTime dt = DateTime.Parse(res[0]);
                    if (_end < dt) continue;
                    if (_start > dt) break;

                    string _p = res[8].Split(',')[0];
                    string _v = res[10].Split(',')[0];
                    _price = Double.Parse(_p);
                    if (j == 0) _close = _price;

                    //strlog = res[0] + "\t" + _p + "\t" + _v + "\n";
                    //file.Write(strlog);

                    _vol += Double.Parse(_v);
                    if (_price > _high) _high = _price;
                    else if (_price < _low) _low = _price;

                    j++;
                }
                _open = _price;

                strlog = dtNow.ToString("MM/dd hh:mm:ss.ffff") + "  O:" + _open + " H:" + _high + " L:" + _low + " C:" + _close + " V:" + _vol + "\n";
                textBox_price.AppendText(strlog);

                file2.Write(strlog);
                file2.Close();
                //file.Write("======================\n");
                //file.Close();

                isReady = false;
                if(dtNow.Hour > 8 && dtNow.Hour < 13) tm.Start();
            }
        }
    }
}
