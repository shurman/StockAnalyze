﻿namespace StockAnalyze
{
    partial class CurrentPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox_price
            // 
            this.textBox_price.Location = new System.Drawing.Point(12, 12);
            this.textBox_price.Multiline = true;
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.ReadOnly = true;
            this.textBox_price.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_price.Size = new System.Drawing.Size(369, 238);
            this.textBox_price.TabIndex = 0;
            // 
            // CurrentPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 262);
            this.Controls.Add(this.textBox_price);
            this.Name = "CurrentPrice";
            this.Text = "CurrentPrice";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_price;

    }
}