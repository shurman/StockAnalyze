﻿namespace StockAnalyze
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            this.MainChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btn_1min = new System.Windows.Forms.Button();
            this.btn_daily = new System.Windows.Forms.Button();
            this.btn_Analyze = new System.Windows.Forms.Button();
            this.textBox_Return = new System.Windows.Forms.TextBox();
            this.textBox_Win = new System.Windows.Forms.TextBox();
            this.textBox_Lose = new System.Windows.Forms.TextBox();
            this.textBox_Long = new System.Windows.Forms.TextBox();
            this.textBox_Short = new System.Windows.Forms.TextBox();
            this.textBox_Gain = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_NegGain = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_backTest = new System.Windows.Forms.Button();
            this.textBox_finReturn = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.combo_index = new System.Windows.Forms.ComboBox();
            this.textBox_start = new System.Windows.Forms.TextBox();
            this.textBox_maxCnt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_1hr = new System.Windows.Forms.Button();
            this.textBox_maxGain = new System.Windows.Forms.TextBox();
            this.textBox_maxLose = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_risk = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_MDD = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.testbtn = new System.Windows.Forms.Button();
            this.textBox_empty = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox_avgGain = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btntest2 = new System.Windows.Forms.Button();
            this.textBox_risk2 = new System.Windows.Forms.TextBox();
            this.btn_Analyze2 = new System.Windows.Forms.Button();
            this.textBox_nextaction = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.MainChart)).BeginInit();
            this.SuspendLayout();
            // 
            // MainChart
            // 
            this.MainChart.BackColor = System.Drawing.Color.Black;
            chartArea1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            chartArea1.InnerPlotPosition.Auto = false;
            chartArea1.InnerPlotPosition.Height = 75F;
            chartArea1.InnerPlotPosition.Width = 95F;
            chartArea1.InnerPlotPosition.X = 5F;
            chartArea1.Name = "StockArea";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 62F;
            chartArea1.Position.Width = 99F;
            chartArea1.Position.X = 0.5F;
            chartArea1.Position.Y = 3F;
            chartArea2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            chartArea2.InnerPlotPosition.Auto = false;
            chartArea2.InnerPlotPosition.Height = 85F;
            chartArea2.InnerPlotPosition.Width = 95F;
            chartArea2.InnerPlotPosition.X = 5F;
            chartArea2.Name = "VolumeArea";
            chartArea2.Position.Auto = false;
            chartArea2.Position.Height = 41F;
            chartArea2.Position.Width = 99F;
            chartArea2.Position.X = 0.5F;
            chartArea2.Position.Y = 59F;
            chartArea3.AxisX.ScrollBar.Enabled = false;
            chartArea3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            chartArea3.InnerPlotPosition.Auto = false;
            chartArea3.InnerPlotPosition.Height = 85F;
            chartArea3.InnerPlotPosition.Width = 95F;
            chartArea3.InnerPlotPosition.X = 5F;
            chartArea3.Name = "MACDArea";
            chartArea3.Position.Auto = false;
            chartArea3.Position.Height = 41F;
            chartArea3.Position.Width = 99F;
            chartArea3.Position.X = 0.5F;
            chartArea3.Position.Y = 59F;
            chartArea4.AxisX.ScrollBar.Enabled = false;
            chartArea4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            chartArea4.InnerPlotPosition.Auto = false;
            chartArea4.InnerPlotPosition.Height = 85F;
            chartArea4.InnerPlotPosition.Width = 95F;
            chartArea4.InnerPlotPosition.X = 5F;
            chartArea4.Name = "RSIArea";
            chartArea4.Position.Auto = false;
            chartArea4.Position.Height = 41F;
            chartArea4.Position.Width = 99F;
            chartArea4.Position.X = 0.5F;
            chartArea4.Position.Y = 59F;
            chartArea5.AxisX.ScrollBar.Enabled = false;
            chartArea5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            chartArea5.InnerPlotPosition.Auto = false;
            chartArea5.InnerPlotPosition.Height = 85F;
            chartArea5.InnerPlotPosition.Width = 95F;
            chartArea5.InnerPlotPosition.X = 5F;
            chartArea5.Name = "CCIArea";
            chartArea5.Position.Auto = false;
            chartArea5.Position.Height = 41F;
            chartArea5.Position.Width = 99F;
            chartArea5.Position.X = 0.5F;
            chartArea5.Position.Y = 59F;
            this.MainChart.ChartAreas.Add(chartArea1);
            this.MainChart.ChartAreas.Add(chartArea2);
            this.MainChart.ChartAreas.Add(chartArea3);
            this.MainChart.ChartAreas.Add(chartArea4);
            this.MainChart.ChartAreas.Add(chartArea5);
            this.MainChart.Location = new System.Drawing.Point(0, 0);
            this.MainChart.Margin = new System.Windows.Forms.Padding(0);
            this.MainChart.Name = "MainChart";
            this.MainChart.Size = new System.Drawing.Size(1024, 680);
            this.MainChart.TabIndex = 0;
            this.MainChart.Text = "chart1";
            this.MainChart.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ChartAxisViewChanged);
            // 
            // btn_1min
            // 
            this.btn_1min.Location = new System.Drawing.Point(1038, 13);
            this.btn_1min.Name = "btn_1min";
            this.btn_1min.Size = new System.Drawing.Size(40, 23);
            this.btn_1min.TabIndex = 1;
            this.btn_1min.Text = "30分";
            this.btn_1min.UseVisualStyleBackColor = true;
            this.btn_1min.Click += new System.EventHandler(this.btn30minClick);
            // 
            // btn_daily
            // 
            this.btn_daily.Location = new System.Drawing.Point(1127, 13);
            this.btn_daily.Name = "btn_daily";
            this.btn_daily.Size = new System.Drawing.Size(40, 23);
            this.btn_daily.TabIndex = 1;
            this.btn_daily.Text = "日";
            this.btn_daily.UseVisualStyleBackColor = true;
            this.btn_daily.Click += new System.EventHandler(this.btnDailyClick);
            // 
            // btn_Analyze
            // 
            this.btn_Analyze.Location = new System.Drawing.Point(1041, 95);
            this.btn_Analyze.Name = "btn_Analyze";
            this.btn_Analyze.Size = new System.Drawing.Size(104, 23);
            this.btn_Analyze.TabIndex = 1;
            this.btn_Analyze.Text = "破N根K棒 (hr)";
            this.btn_Analyze.UseVisualStyleBackColor = true;
            this.btn_Analyze.Click += new System.EventHandler(this.btn_analyzeClick);
            // 
            // textBox_Return
            // 
            this.textBox_Return.Location = new System.Drawing.Point(1041, 247);
            this.textBox_Return.Name = "textBox_Return";
            this.textBox_Return.ReadOnly = true;
            this.textBox_Return.Size = new System.Drawing.Size(40, 22);
            this.textBox_Return.TabIndex = 2;
            // 
            // textBox_Win
            // 
            this.textBox_Win.Location = new System.Drawing.Point(1040, 294);
            this.textBox_Win.Name = "textBox_Win";
            this.textBox_Win.ReadOnly = true;
            this.textBox_Win.Size = new System.Drawing.Size(40, 22);
            this.textBox_Win.TabIndex = 2;
            // 
            // textBox_Lose
            // 
            this.textBox_Lose.Location = new System.Drawing.Point(1086, 294);
            this.textBox_Lose.Name = "textBox_Lose";
            this.textBox_Lose.ReadOnly = true;
            this.textBox_Lose.Size = new System.Drawing.Size(40, 22);
            this.textBox_Lose.TabIndex = 2;
            // 
            // textBox_Long
            // 
            this.textBox_Long.Location = new System.Drawing.Point(1040, 341);
            this.textBox_Long.Name = "textBox_Long";
            this.textBox_Long.ReadOnly = true;
            this.textBox_Long.Size = new System.Drawing.Size(40, 22);
            this.textBox_Long.TabIndex = 2;
            // 
            // textBox_Short
            // 
            this.textBox_Short.Location = new System.Drawing.Point(1086, 341);
            this.textBox_Short.Name = "textBox_Short";
            this.textBox_Short.ReadOnly = true;
            this.textBox_Short.Size = new System.Drawing.Size(40, 22);
            this.textBox_Short.TabIndex = 2;
            // 
            // textBox_Gain
            // 
            this.textBox_Gain.Location = new System.Drawing.Point(1040, 387);
            this.textBox_Gain.Name = "textBox_Gain";
            this.textBox_Gain.ReadOnly = true;
            this.textBox_Gain.Size = new System.Drawing.Size(40, 22);
            this.textBox_Gain.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(1041, 231);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "毛損益";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1053, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "勝";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(1099, 279);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "負";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1053, 326);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "多";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1099, 326);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "空";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(1053, 372);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "益";
            // 
            // textBox_NegGain
            // 
            this.textBox_NegGain.Location = new System.Drawing.Point(1086, 387);
            this.textBox_NegGain.Name = "textBox_NegGain";
            this.textBox_NegGain.ReadOnly = true;
            this.textBox_NegGain.Size = new System.Drawing.Size(40, 22);
            this.textBox_NegGain.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(1099, 372);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 3;
            this.label7.Text = "損";
            // 
            // btn_backTest
            // 
            this.btn_backTest.Location = new System.Drawing.Point(1041, 146);
            this.btn_backTest.Name = "btn_backTest";
            this.btn_backTest.Size = new System.Drawing.Size(51, 23);
            this.btn_backTest.TabIndex = 1;
            this.btn_backTest.Text = "權益圖";
            this.btn_backTest.UseVisualStyleBackColor = true;
            this.btn_backTest.Click += new System.EventHandler(this.btn_backtestClick);
            // 
            // textBox_finReturn
            // 
            this.textBox_finReturn.Location = new System.Drawing.Point(1086, 247);
            this.textBox_finReturn.Name = "textBox_finReturn";
            this.textBox_finReturn.ReadOnly = true;
            this.textBox_finReturn.Size = new System.Drawing.Size(40, 22);
            this.textBox_finReturn.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(1086, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 3;
            this.label8.Text = "淨損益";
            // 
            // combo_index
            // 
            this.combo_index.FormattingEnabled = true;
            this.combo_index.Items.AddRange(new object[] {
            "Volume",
            "MACD",
            "CCI",
            "RSI"});
            this.combo_index.Location = new System.Drawing.Point(1041, 591);
            this.combo_index.Name = "combo_index";
            this.combo_index.Size = new System.Drawing.Size(64, 20);
            this.combo_index.TabIndex = 4;
            this.combo_index.Text = "RSI";
            this.combo_index.SelectedIndexChanged += new System.EventHandler(this.index_selected);
            // 
            // textBox_start
            // 
            this.textBox_start.Location = new System.Drawing.Point(1041, 66);
            this.textBox_start.Name = "textBox_start";
            this.textBox_start.Size = new System.Drawing.Size(40, 22);
            this.textBox_start.TabIndex = 2;
            // 
            // textBox_maxCnt
            // 
            this.textBox_maxCnt.Location = new System.Drawing.Point(1089, 66);
            this.textBox_maxCnt.Name = "textBox_maxCnt";
            this.textBox_maxCnt.Size = new System.Drawing.Size(40, 22);
            this.textBox_maxCnt.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(1041, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 12);
            this.label9.TabIndex = 3;
            this.label9.Text = "Start At";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(1087, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 12);
            this.label10.TabIndex = 3;
            this.label10.Text = "Max Cnt";
            // 
            // btn_1hr
            // 
            this.btn_1hr.Location = new System.Drawing.Point(1083, 13);
            this.btn_1hr.Name = "btn_1hr";
            this.btn_1hr.Size = new System.Drawing.Size(40, 23);
            this.btn_1hr.TabIndex = 1;
            this.btn_1hr.Text = "60分";
            this.btn_1hr.UseVisualStyleBackColor = true;
            this.btn_1hr.Click += new System.EventHandler(this.btn1hrClick);
            // 
            // textBox_maxGain
            // 
            this.textBox_maxGain.Location = new System.Drawing.Point(1086, 436);
            this.textBox_maxGain.Name = "textBox_maxGain";
            this.textBox_maxGain.ReadOnly = true;
            this.textBox_maxGain.Size = new System.Drawing.Size(40, 22);
            this.textBox_maxGain.TabIndex = 2;
            // 
            // textBox_maxLose
            // 
            this.textBox_maxLose.Location = new System.Drawing.Point(1040, 484);
            this.textBox_maxLose.Name = "textBox_maxLose";
            this.textBox_maxLose.ReadOnly = true;
            this.textBox_maxLose.Size = new System.Drawing.Size(40, 22);
            this.textBox_maxLose.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(1086, 420);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 3;
            this.label11.Text = "Max 益";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(1040, 469);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 3;
            this.label12.Text = "Max 損";
            // 
            // textBox_risk
            // 
            this.textBox_risk.Location = new System.Drawing.Point(1075, 526);
            this.textBox_risk.Name = "textBox_risk";
            this.textBox_risk.ReadOnly = true;
            this.textBox_risk.Size = new System.Drawing.Size(40, 22);
            this.textBox_risk.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(1029, 529);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 24);
            this.label13.TabIndex = 3;
            this.label13.Text = "賠率\r\n(>=1)";
            // 
            // textBox_MDD
            // 
            this.textBox_MDD.Location = new System.Drawing.Point(1086, 484);
            this.textBox_MDD.Name = "textBox_MDD";
            this.textBox_MDD.ReadOnly = true;
            this.textBox_MDD.Size = new System.Drawing.Size(40, 22);
            this.textBox_MDD.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(1092, 469);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 12);
            this.label14.TabIndex = 3;
            this.label14.Text = "MDD";
            // 
            // testbtn
            // 
            this.testbtn.Location = new System.Drawing.Point(1037, 638);
            this.testbtn.Name = "testbtn";
            this.testbtn.Size = new System.Drawing.Size(32, 23);
            this.testbtn.TabIndex = 1;
            this.testbtn.Text = "test";
            this.testbtn.UseVisualStyleBackColor = true;
            this.testbtn.Click += new System.EventHandler(this.btn_testClick);
            // 
            // textBox_empty
            // 
            this.textBox_empty.Location = new System.Drawing.Point(1075, 554);
            this.textBox_empty.Name = "textBox_empty";
            this.textBox_empty.ReadOnly = true;
            this.textBox_empty.Size = new System.Drawing.Size(40, 22);
            this.textBox_empty.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(1029, 559);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 3;
            this.label15.Text = "空手數";
            // 
            // textBox_avgGain
            // 
            this.textBox_avgGain.Location = new System.Drawing.Point(1040, 436);
            this.textBox_avgGain.Name = "textBox_avgGain";
            this.textBox_avgGain.ReadOnly = true;
            this.textBox_avgGain.Size = new System.Drawing.Size(40, 22);
            this.textBox_avgGain.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(1040, 420);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 12);
            this.label16.TabIndex = 3;
            this.label16.Text = "Avg 益";
            // 
            // btntest2
            // 
            this.btntest2.Location = new System.Drawing.Point(1075, 638);
            this.btntest2.Name = "btntest2";
            this.btntest2.Size = new System.Drawing.Size(40, 23);
            this.btntest2.TabIndex = 1;
            this.btntest2.Text = "test2";
            this.btntest2.UseVisualStyleBackColor = true;
            this.btntest2.Click += new System.EventHandler(this.btn_test2Click);
            // 
            // textBox_risk2
            // 
            this.textBox_risk2.Location = new System.Drawing.Point(1121, 526);
            this.textBox_risk2.Name = "textBox_risk2";
            this.textBox_risk2.ReadOnly = true;
            this.textBox_risk2.Size = new System.Drawing.Size(40, 22);
            this.textBox_risk2.TabIndex = 5;
            // 
            // btn_Analyze2
            // 
            this.btn_Analyze2.Location = new System.Drawing.Point(1041, 120);
            this.btn_Analyze2.Name = "btn_Analyze2";
            this.btn_Analyze2.Size = new System.Drawing.Size(104, 23);
            this.btn_Analyze2.TabIndex = 6;
            this.btn_Analyze2.Text = "順勢 (day)";
            this.btn_Analyze2.UseVisualStyleBackColor = true;
            this.btn_Analyze2.Click += new System.EventHandler(this.btn_analyzeClick2);
            // 
            // textBox_nextaction
            // 
            this.textBox_nextaction.Location = new System.Drawing.Point(1041, 192);
            this.textBox_nextaction.Name = "textBox_nextaction";
            this.textBox_nextaction.Size = new System.Drawing.Size(104, 22);
            this.textBox_nextaction.TabIndex = 7;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(1071, 177);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 8;
            this.label17.Text = "下一步";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1174, 682);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBox_nextaction);
            this.Controls.Add(this.btn_Analyze2);
            this.Controls.Add(this.textBox_risk2);
            this.Controls.Add(this.combo_index);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_maxCnt);
            this.Controls.Add(this.textBox_MDD);
            this.Controls.Add(this.textBox_empty);
            this.Controls.Add(this.textBox_risk);
            this.Controls.Add(this.textBox_maxLose);
            this.Controls.Add(this.textBox_start);
            this.Controls.Add(this.textBox_avgGain);
            this.Controls.Add(this.textBox_NegGain);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox_maxGain);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_Gain);
            this.Controls.Add(this.textBox_Short);
            this.Controls.Add(this.textBox_Long);
            this.Controls.Add(this.textBox_Lose);
            this.Controls.Add(this.textBox_Win);
            this.Controls.Add(this.textBox_finReturn);
            this.Controls.Add(this.textBox_Return);
            this.Controls.Add(this.btntest2);
            this.Controls.Add(this.testbtn);
            this.Controls.Add(this.btn_backTest);
            this.Controls.Add(this.btn_Analyze);
            this.Controls.Add(this.btn_daily);
            this.Controls.Add(this.btn_1hr);
            this.Controls.Add(this.btn_1min);
            this.Controls.Add(this.MainChart);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Stock Analyze";
            ((System.ComponentModel.ISupportInitialize)(this.MainChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart MainChart;
        private System.Windows.Forms.Button btn_1min;
        private System.Windows.Forms.Button btn_daily;
        private System.Windows.Forms.Button btn_Analyze;
        private System.Windows.Forms.TextBox textBox_Return;
        private System.Windows.Forms.TextBox textBox_Win;
        private System.Windows.Forms.TextBox textBox_Lose;
        private System.Windows.Forms.TextBox textBox_Long;
        private System.Windows.Forms.TextBox textBox_Short;
        private System.Windows.Forms.TextBox textBox_Gain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_NegGain;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_backTest;
        private System.Windows.Forms.TextBox textBox_finReturn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox combo_index;
        private System.Windows.Forms.TextBox textBox_start;
        private System.Windows.Forms.TextBox textBox_maxCnt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_1hr;
        private System.Windows.Forms.TextBox textBox_maxGain;
        private System.Windows.Forms.TextBox textBox_maxLose;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_risk;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_MDD;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button testbtn;
        private System.Windows.Forms.TextBox textBox_empty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox_avgGain;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btntest2;
        private System.Windows.Forms.TextBox textBox_risk2;
        private System.Windows.Forms.Button btn_Analyze2;
        private System.Windows.Forms.TextBox textBox_nextaction;
        private System.Windows.Forms.Label label17;
    }
}

