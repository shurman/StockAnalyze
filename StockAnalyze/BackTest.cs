﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace StockAnalyze
{
    public partial class BackTest : Form
    {
        Series S_TltRes = new Series();
        Series S_Res = new Series();

        double tltRes = 0;

        public BackTest(List<OrderPosition> _Order)
        {
            InitializeComponent();

            S_TltRes.ChartType = SeriesChartType.Line;
            S_Res.ChartType = SeriesChartType.Column;
            chart1.Series.Add(S_TltRes);
            chart1.Series.Add(S_Res);
            S_TltRes.ChartArea = "ChartArea1";
            S_Res.ChartArea = "ChartArea1";
            S_TltRes.Color = Color.Wheat;
            S_Res.Color = Color.Yellow;

            chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = Color.DimGray;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = Color.DimGray;
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.ForeColor = Color.White;
            chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.ForeColor = Color.White;

            chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = Color.DimGray;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = Color.DimGray;
            chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;

            for (int i = 0; i < _Order.Count; i++)
            {
                tltRes+=_Order[i].returnValue;
                DataPoint tmp = new DataPoint(i+1, tltRes);
                DataPoint tmp2 = new DataPoint(i+1, _Order[i].returnValue);

                if (_Order[i].OrderType == OrderPosition.Type.Long) tmp2.Color = Color.Red;
                else tmp2.Color = Color.LawnGreen;

                S_TltRes.Points.Add(tmp);
                S_Res.Points.Add(tmp2);
            }
        }
    }
}
